#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/tianbot/f1_ws/devel:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH='/home/tianbot/f1_ws/devel/lib:/home/tianbot/tianbot_mini_ws/devel/lib:/home/tianbot/manipulation_ws/devel/lib:/home/tianbot/turtlebot_ws/devel/lib:/opt/ros/melodic/lib:/opt/ros/melodic/lib/x86_64-linux-gnu:/home/tianbot/study_ws/devel/lib:/home/tianbot/tianbot_ws/devel/lib:/home/tianbot/catkin_ws/devel/lib'
export PKG_CONFIG_PATH='/home/tianbot/f1_ws/devel/lib/pkgconfig:/home/tianbot/tianbot_mini_ws/devel/lib/pkgconfig:/home/tianbot/manipulation_ws/devel/lib/pkgconfig:/home/tianbot/turtlebot_ws/devel/lib/pkgconfig:/opt/ros/melodic/lib/pkgconfig:/home/tianbot/study_ws/devel/lib/pkgconfig:/home/tianbot/tianbot_ws/devel/lib/pkgconfig:/home/tianbot/catkin_ws/devel/lib/pkgconfig'
export PWD='/home/tianbot/f1_ws/build'
export PYTHONPATH='/home/tianbot/f1_ws/devel/lib/python2.7/dist-packages:/home/tianbot/tianbot_mini_ws/devel/lib/python2.7/dist-packages:/home/tianbot/manipulation_ws/devel/lib/python2.7/dist-packages:/home/tianbot/turtlebot_ws/devel/lib/python2.7/dist-packages:/opt/ros/melodic/lib/python2.7/dist-packages:/home/tianbot/study_ws/devel/lib/python2.7/dist-packages:/home/tianbot/tianbot_ws/devel/lib/python2.7/dist-packages'
export ROSLISP_PACKAGE_DIRECTORIES="/home/tianbot/f1_ws/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/tianbot/f1_ws/src:$ROS_PACKAGE_PATH"