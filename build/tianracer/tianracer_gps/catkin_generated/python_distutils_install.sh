#!/bin/sh

if [ -n "$DESTDIR" ] ; then
    case $DESTDIR in
        /*) # ok
            ;;
        *)
            /bin/echo "DESTDIR argument must be absolute... "
            /bin/echo "otherwise python's distutils will bork things."
            exit 1
    esac
fi

echo_and_run() { echo "+ $@" ; "$@" ; }

echo_and_run cd "/home/tianbot/f1_ws/src/tianracer/tianracer_gps"

# ensure that Python install destination exists
echo_and_run mkdir -p "$DESTDIR/home/tianbot/f1_ws/install/lib/python2.7/dist-packages"

# Note that PYTHONPATH is pulled from the environment to support installing
# into one location when some dependencies were installed in another
# location, #123.
echo_and_run /usr/bin/env \
    PYTHONPATH="/home/tianbot/f1_ws/install/lib/python2.7/dist-packages:/home/tianbot/f1_ws/build/lib/python2.7/dist-packages:$PYTHONPATH" \
    CATKIN_BINARY_DIR="/home/tianbot/f1_ws/build" \
    "/usr/bin/python2" \
    "/home/tianbot/f1_ws/src/tianracer/tianracer_gps/setup.py" \
     \
    build --build-base "/home/tianbot/f1_ws/build/tianracer/tianracer_gps" \
    install \
    --root="${DESTDIR-/}" \
    --install-layout=deb --prefix="/home/tianbot/f1_ws/install" --install-scripts="/home/tianbot/f1_ws/install/bin"
