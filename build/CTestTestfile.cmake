# CMake generated Testfile for 
# Source directory: /home/tianbot/f1_ws/src
# Build directory: /home/tianbot/f1_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("tianracer/tianracer")
subdirs("tianracer/tianracer_gps")
subdirs("tianracer/tianracer_jetson")
subdirs("tianracer/tianracer_rviz")
subdirs("tianracer/tianracer_test")
subdirs("tianracer/tianracer_bringup")
subdirs("tianracer/tianracer_description")
subdirs("tianracer/tianracer_slam")
subdirs("tianracer/tianracer_teleop")
subdirs("f1tenth_simulator")
subdirs("tianracer/tianracer_core")
subdirs("tianracer/tianracer_navigation")
subdirs("xiaomu_nav")
